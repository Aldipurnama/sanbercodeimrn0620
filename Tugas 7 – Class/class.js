console.log("\n")
var hai = "1. Animal Class"
console.log(hai)
console.log("\n")
//
class Animal {
    constructor(name, legs = 4, cold_blooded = false){
        this.name = name;
        this.legs = legs;
        this.cold_blooded = cold_blooded;
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log("\n")
var hai = "Code class Ape dan class Frog di sini" 
console.log(hai)
console.log("\n")
// 
class Ape extends Animal {
    constructor(name, legs = 2, cold_blooded = false){
        super(name, 2, cold_blooded)
    }

    yell() {
        console.log('Auooo')
    }
}

class Frog extends Animal {
    constructor(name, legs = 4, cold_blooded = false){
        super(name, legs, cold_blooded)
    }

    jump() {
        console.log('hop hop')
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop"

console.log("\n")
var hai = "2. Function to Class" 
console.log(hai)
console.log("\n")
//
class Clock {
    constructor(obj = {}){
        this.template = obj.template || 'h:m:s'
        this.timer = null
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        let self = this
        self.render();
        self.timer = setInterval(() => self.render(), 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();