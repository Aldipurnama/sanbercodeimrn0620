var hai = "LOOPING PERTAMA" 
console.log(hai)

var i=2;
while(i < 21) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log(i + " " + '- I Love Coding'); // Menampilkan nilai flag pada iterasi tertentu
  i=i+2; // Mengubah nilai flag dengan menambahkan 1
}

console.log(" ")
var hai = "LOOPING KEDUA" 
console.log(hai)

var i=20;
while(i>0) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log(i + " " + '- I will become a mobile developer'); // Menampilkan nilai flag pada iterasi tertentu
  i=i-2; // Mengubah nilai flag dengan menambahkan 1
}

//
console.log(" ")
var hai = "No. 2 Looping Menggunakan for" 
console.log(hai)

for(var angka=1; angka<21; angka++){
if( angka % 2 == 0 ){
    console.log( angka + " - Berkualitas" )
} else if( angka % 3 == 0 ) {
    console.log( angka + " - I Love Coding" )
} else{
    console.log( angka + " - Santai" )
}
}

//
console.log(" ")
var hai = "No. 3 Membuat Persegi Panjang" 
console.log(hai)

i = 0; 
while (i < 4) {
    console.log("########");
    i++;
}

//
console.log(" ")
var hai = "No. 4 Membuat Tangga" 
console.log(hai)

var y2 = 7;

var stair = "";

for (i = 1; i <= y2; i++) {
    stair = stair.concat('#');
    console.log(stair);
};

//
console.log(" ")
var hai = "No. 5 Membuat Papan Catur" 
console.log(hai)

var x3 = 8;
var y3 = 8;

chessBoard = '';

for (i = 1; i <= y3; i++) {
    for (j = 1; j <= x3; j++) {
        if ((i + j) % 2 == 0) {
            chessBoard = chessBoard.concat(' ');
        } else {
            chessBoard = chessBoard.concat('#');
        }
    };
    if (i <= y3 - 1) {
        chessBoard = chessBoard.concat('\n');
    }
};

console.log(chessBoard);