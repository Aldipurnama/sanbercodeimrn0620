console.log("\n Soal No. 1 Mengubah fungsi menjadi fungsi arrow \n");

const golden = ()=>{
    console.log("this is golden!!")
  }
golden();


console.log("\n Soal No 2 Sederhanakan menjadi Object literal di ES6 \n")

const newFunction =(firstName, lastName)=>{
    return {
      firstName,
      lastName,
      fullName: function(){
        console.log(`${firstName} ${lastName}`);
        return 
      }
    }
  }
  //Driver Code 
newFunction("William", "Imoh").fullName();


console.log("\n Soal No 3 Destructuring \n")

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  const {firstName,lastName,destination,occupation}= newObject;
  // Driver code
console.log(firstName, lastName, destination, occupation)

console.log("\n Soal No 4 Array Spreading \n")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east];
//Driver Code
console.log(combined)


console.log("\n Soal no 5 Template Literals \n")

const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
    
 
// Driver Code
console.log(before)