////Tugas Conditional
/// If-else
// kondisi 1

var nama1 = ""
var peran = ""

if( nama1 == "" ) {
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Nama harus diisi!" )
} else if( peran == ""){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Halo " + nama1 + ", Pilih peranmu untuk memulai game!" )
} else if( peran == "Penyihir" ){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Selamat datang di Dunia Warewolf, "+ nama1 )
    console.log( "Halo Penyihir" + nama1 + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if( peran == "Guard" ){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Selamat datang di Dunia Warewolf, "+ nama1 )
    console.log( "Halo Guard" + nama1 + ", kamu akan membantu melindungi temanmu dari serangan werewolf!" )
} else if( peran == "Werewolf" ){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Selamat datang di Dunia Warewolf, "+ nama1)
    console.log( "Halo Werewolf" + nama1 + ", Kamu akan memakan mangsa setiap malam!" )
}

console.log(" ")
// kondisi 2

var nama1 = "Jhon"
var peran = ""

if( nama1 == "" ) {
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Nama harus diisi!" )
} else if( peran == ""){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Halo " + nama1 + ", Pilih peranmu untuk memulai game!" )
} else if( peran == "Penyihir" ){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Selamat datang di Dunia Warewolf, "+ nama1 )
    console.log( "Halo Penyihir" + nama1 + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if( peran == "Guard" ){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Selamat datang di Dunia Warewolf, "+ nama1 )
    console.log( "Halo Guard" + nama1 + ", kamu akan membantu melindungi temanmu dari serangan werewolf!" )
} else if( peran == "Werewolf" ){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Selamat datang di Dunia Warewolf, "+ nama1)
    console.log( "Halo Werewolf" + nama1 + ", Kamu akan memakan mangsa setiap malam!" )
}

console.log(" ")
// kondisi 3

var nama1 = "Jane"
var peran = "Penyihir"

if( nama1 == "" ) {
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Nama harus diisi!" )
} else if( peran == ""){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Halo " + nama1 + ", Pilih peranmu untuk memulai game!" )
} else if( peran == "Penyihir" ){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Selamat datang di Dunia Warewolf, "+ nama1 )
    console.log( "Halo Penyihir" + nama1 + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if( peran == "Guard" ){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Selamat datang di Dunia Warewolf, "+ nama1 )
    console.log( "Halo Guard" + nama1 + ", kamu akan membantu melindungi temanmu dari serangan werewolf!" )
} else if( peran == "Werewolf" ){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Selamat datang di Dunia Warewolf, "+ nama1)
    console.log( "Halo Werewolf" + nama1 + ", Kamu akan memakan mangsa setiap malam!" )
}

console.log(" ")
// kondisi 4

var nama1 = "Jenita"
var peran = "Guard"

if( nama1 == "" ) {
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Nama harus diisi!" )
} else if( peran == ""){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Halo " + nama1 + ", Pilih peranmu untuk memulai game!" )
} else if( peran == "Penyihir" ){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Selamat datang di Dunia Warewolf, "+ nama1 )
    console.log( "Halo Penyihir" + nama1 + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if( peran == "Guard" ){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Selamat datang di Dunia Warewolf, "+ nama1 )
    console.log( "Halo Guard" + nama1 + ", kamu akan membantu melindungi temanmu dari serangan werewolf!" )
} else if( peran == "Werewolf" ){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Selamat datang di Dunia Warewolf, "+ nama1)
    console.log( "Halo Werewolf" + nama1 + ", Kamu akan memakan mangsa setiap malam!" )
}

console.log(" ")
// kondisi 5

var nama1 = "Junaedi"
var peran = "Werewolf"

if( nama1 == "" ) {
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Nama harus diisi!" )
} else if( peran == ""){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Halo " + nama1 + ", Pilih peranmu untuk memulai game!" )
} else if( peran == "Penyihir" ){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Selamat datang di Dunia Warewolf, "+ nama1 )
    console.log( "Halo Penyihir" + nama1 + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if( peran == "Guard" ){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Selamat datang di Dunia Warewolf, "+ nama1 )
    console.log( "Halo Guard" + nama1 + ", kamu akan membantu melindungi temanmu dari serangan werewolf!" )
} else if( peran == "Werewolf" ){
    console.log( "// Output untuk Input nama = "+ nama1 + " dan peran = "+peran )
    console.log( "Selamat datang di Dunia Warewolf, "+ nama1)
    console.log( "Halo Werewolf" + nama1 + ", Kamu akan memakan mangsa setiap malam!" )
}


 console.log(" ")
///Switch Case

 var hari = 21; 
 var bulan = 1; 
 var tahun = 1945;
 var kosong ="bulan tidak ada";
 switch (bulan) {
     case 1:
         console.log(hari + " " + 'Januari' + " " + tahun)
         break;
     case 2:
         console.log(hari + " " + 'Februari' + " " + tahun)
            break;
     case 3:
         console.log(hari + " " + 'Maret' + " " + tahun)
            break;
            case 2:
     case 4:
         console.log(hari + " " + 'April' + " " + tahun)
                    break;
     case 5:
         console.log(hari + " " + 'Mei' + " " + tahun)
         break;
     case 6:
         console.log(hari + " " + 'Juni' + " " + tahun)
         break;
     case 7:
         console.log(hari + " " + "Juli" + " " + tahun)
         break;
     case 8:
         console.log(hari + " " + "Agustus" + " " + tahun)
         break;
     case 9:
         console.log(hari + " " + "September"+ " " + tahun)
         break;
     case 10:
         console.log(hari + " " + "Oktober" + " " + tahun)
         break;
     case 11:
         console.log(hari + " " + "November" + " " + tahun)
         break;
     case 12:
         console.log(hari + " " + "Desember" + " " + tahun)
     default:
         console.log("kosong")
         break;
 }