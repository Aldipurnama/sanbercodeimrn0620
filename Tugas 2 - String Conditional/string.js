//Soal no.1 Membuat Kalimat

console.log('Soal No. 1')
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var stringGabung = word + " " + second + " " + third + " " + fourth + " " + fifth + " " + sixth + " " + seventh
console.log(stringGabung)

//Soal no.2 Mengurai kalimat (Akses karakter dalam string)
console.log('Soal No. 2')
var sentence = "I am going to be React Native Developer"; 
var FirstWord = sentence[0] ; 
var SecondWord = sentence[2] + sentence[3]  ; 
var ThirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9]  ; 
var FourthWord = sentence[11] + sentence[12]  ; 
var FifthWord = sentence[14] + sentence[15]  ; 
var SixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21]  ;
var SeventhWord = sentence[30]+ sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] +sentence[38]  ;
console.log('First Word: ' + FirstWord); 
console.log('Second Word: ' + SecondWord); 
console.log('Third Word: ' + ThirdWord); 
console.log('Fourth Word: ' + FourthWord); 
console.log('Fifth Word: ' + FifthWord); 
console.log('Sixth Word: ' + SixthWord); 
console.log('Seventh Word: ' + SeventhWord);


//Soal No. 3 Mengurai Kalimat (Substring)
console.log('Soal No. 3')
var sentence2 = 'wow JavaScript is so cool'; 
var FirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 15);
var thirdWord2 = sentence2.substring(15, 18);
var fourthWord2 = sentence2.substring(18, 21);
var fifthWord2 = sentence2.substring(21, 26);
console.log('First Word: ' + FirstWord2); 
console.log('Second Word: ' + secondWord2);
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);


//Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String
console.log('Soal No. 4')
var sentence3 = 'wow JavaScript is so cool'; 
var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14);
var thirdWord3 = sentence3.substring(15, 17);
var fourthWord3 = sentence3.substring(18, 20);
var fifthWord3 = sentence3.substring(21, 26);

var firstWordLength = exampleFirstWord3.length  
var secondWordLength = secondWord3.length
var thirdWordLength = thirdWord3.length
var fourthWordLength = fourthWord3.length
var fifthWordLength = fifthWord3.length
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength);
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength);