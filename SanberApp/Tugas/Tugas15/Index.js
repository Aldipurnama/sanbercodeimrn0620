import React from "react";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {createDrawerNavigator} from "@react-navigation/drawer";
import { StyleSheet, Text, View } from "react-native";
import {SignIn,CreateAccount,Home,Search,Details,Search2,Profile,Splash} from './screen';
import {AuthContext} from "./context";

const AuthStack=createStackNavigator();
const Tabs=createBottomTabNavigator();
const HomeStack=createStackNavigator();
const SearchStack=createStackNavigator();
const ProfileStack=createStackNavigator();
const RootStack=createStackNavigator();

const ProfileStackScreen=()=>(
  <ProfileStack.Navigator>
    <ProfileStack.Screen name="Profile" component={Profile}/>
  </ProfileStack.Navigator>
)
const Drawer=createDrawerNavigator();
const HomeStackScreen=()=>{
  return(<HomeStack.Navigator>
    <HomeStack.Screen name="Home" component={Home}></HomeStack.Screen>
    <HomeStack.Screen name="Details" component={Details} options={({route})=>{
      return{
        title:route.params.name
      }
    }}></HomeStack.Screen>
  </HomeStack.Navigator>)
}
const SearchStackScreen=()=>{
  return(<SearchStack.Navigator>
    <SearchStack.Screen name="Search" component={Search}></SearchStack.Screen>
    <SearchStack.Screen name="Search2" component={Search2}></SearchStack.Screen>
  </SearchStack.Navigator>)
}

const TabsScreen=()=>(
  <Tabs.Navigator>
       <Tabs.Screen name="Home" component={HomeStackScreen}></Tabs.Screen>
       <Tabs.Screen name="Search" component={SearchStackScreen}></Tabs.Screen>
  </Tabs.Navigator>
)
const DrawerScreen=()=>(
  <Drawer.Navigator initialRouteName="Profile">
        <Drawer.Screen name="Home" component={TabsScreen}/>
        <Drawer.Screen name="Profile" component={ProfileStackScreen}/>
  </Drawer.Navigator>
)
const AuthStackScreen=()=>(
    
  <AuthStack.Navigator>
    <AuthStack.Screen name="SignIn" component={SignIn} options={{title:'Sign in'}}/>
    <AuthStack.Screen name="CreateAccount" component={CreateAccount} options={{title:'Create Account'}}/>
</AuthStack.Navigator>
)
const RootStackScreen=({userToken})=>
  (<RootStack.Navigator>
    {userToken?(
    
      <RootStack.Screen name="App" component={DrawerScreen} options={
        {animationEnabled:false}
      }/>
    ):(
      
      <RootStack.Screen name="Auth" component={AuthStackScreen} options={{
        animationEnabled:false
      }}/>
    )}
    
  </RootStack.Navigator>)

export default ()=>{
  const [isLoading,setIsLoading]=React.useState(true);
  const [userToken,setUserToken]=React.useState(null);

  const authContext=React.useMemo(()=>{
    return{
      signIn:()=>{
          setIsLoading(false)
          setUserToken("asd")
      },
      signUp:()=>{
        setIsLoading(false)
        setUserToken("asd")
        
      },
      signOut:()=>{
        
        setIsLoading(false)
        setUserToken(null)
        
      }
    }
  })
  React.useEffect(()=>{
    setTimeout(() => {
      setIsLoading(false)
    }, 1000);
  })
  if(isLoading)
    return <Splash/>
    
  return(
    <AuthContext.Provider value={authContext}>
         <NavigationContainer>
     <RootStackScreen userToken={userToken}/>
      
   </NavigationContainer>
   </AuthContext.Provider>

  )
}


