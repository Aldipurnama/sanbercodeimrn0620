import React,{Component, useContext} from 'react';
import {LinearGradient} from 'expo-linear-gradient';
import{
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    FlatList,
    Button,
    
} from 'react-native'
import Icon from "react-native-vector-icons/MaterialIcons";
import {AuthContext} from "./SkillData"
class LoginScreen extends Component{
    render(){
        return(
            <View style={styles.container}>
                <LinearGradient colors={['#FFE4C4','#7DBFBB']} style={styles.logo}>
                    <Text style={styles.logoTitle}>AL</Text>
                </LinearGradient>
                <View style={styles.menu}> 
                    <Text style={{...styles.menuItems,...styles.menuItemsActive}}>Login</Text>
                    <Text style={styles.menuItems}>Register</Text>
                </View>
                <View style={{marginVertical:40,flexDirection:'column',width:'70%'}}>
                    <Text style={{fontFamily:'Roboto',fontSize:20}}> User Name</Text>
                    <View style={styles.form}></View>
                </View>
                <View style={{marginVertical:10,flexDirection:'column',width:'70%'}}>
                    <Text style={{fontFamily:'Roboto',fontSize:20}}> Password</Text>
                    <View style={styles.form}></View>
                </View>
                <TouchableOpacity style={styles.buttonLogin} onPress={()=>this.props.context.login()}>
                    <Text style={{fontSize:25,color:'white'}}>Login</Text>
                </TouchableOpacity>
                <Text style={{}}>
                    Login With
                </Text>
                <View style={{flexDirection:'row'}}>
                      <Image source={require('./images/Facebook.png')} height={50} width={50} style={styles.images}></Image>
                      <Image source={require('./images/Twitter.png')} style={{...styles.images,height:55,width:55}}></Image>
                     </View>
            </View>
        )
    }
}
export default()=>{
    let cont=React.useContext(AuthContext)
    return(<LoginScreen context={cont}/>)
}
const styles=StyleSheet.create({
    images:{
        margin:15
    
    },
    buttonLogin:{
        justifyContent:'center',
        alignItems:'center',
        marginVertical:30,
        width:'70%',
        height:50,
        backgroundColor:'#5F9EA0',
        borderRadius:200
    }, 
    form:{
        width:'100%',
        backgroundColor:'#FFE4C4',
        height:40,
        borderColor:'#FFE4C4',
        borderRadius:10,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 12},
        shadowOpacity: 1,
        shadowRadius:20,
        elevation: 20,
    },
    menuItems:{
        fontWeight:'400',
        fontSize:30,
        paddingHorizontal:"10%",
        
    },
    menuItemsActive:{
        borderBottomColor:'#00CCC0',
        borderBottomWidth:5 
    },
    menu:{
        marginTop:20,
        flexDirection:'row',
       
    },  
    logoTitle:{
        fontSize:40,
        color:'white',
        textAlign:'center',
        paddingVertical:10,
    },
    container:{
        
        fontFamily:'Roboto',
        flex:1,
        alignItems:'center',
        marginTop:'20%'
    },
    logo:{
        height:85,
        width:85,
        borderRadius:10,
        
        shadowOffset: {width: 0, height: 22},
        shadowOpacity: 1,
        shadowRadius:20,
        elevation: 20,
    }
})
