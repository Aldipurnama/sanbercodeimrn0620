import React,{Component} from 'react'
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import {createDrawerNavigator} from '@react-navigation/drawer'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import Login from "./LoginScreen"
import SkillScreen from './SkillScreen';
import ProjectScreen from './ProjectScreen';
import AddScreen from './AddScreen';
import AboutScreen from './AboutScreen'
import {AuthContext} from './SkillData'
const LoginStack=createStackNavigator();
const Drawer=createDrawerNavigator();
const Root=createStackNavigator();
const BottomTab=createBottomTabNavigator();
const BottomTabScreen=()=>(
    <BottomTab.Navigator>
        <BottomTab.Screen name="SkillScreen" component={SkillScreen}/>
        <BottomTab.Screen name="ProjectScreen" component={ProjectScreen}/>
        <BottomTab.Screen name="AddScreen" component={AddScreen}/>
        
    </BottomTab.Navigator>
)
const DrawerScreen=()=>(
    <Drawer.Navigator headerMode={false}>
        <Drawer.Screen name="Tabs" component={BottomTabScreen}/>
        <Drawer.Screen name="AboutScreen" component={AboutScreen}/>
    </Drawer.Navigator>
)
const RootScreen=({isLogin})=>isLogin?(<DrawerScreen/>):(<LoginScreen/>)
// const 
const LoginScreen=()=>(<LoginStack.Navigator headerMode={false}>
    <LoginStack.Screen name="Login" component={Login}></LoginStack.Screen>
</LoginStack.Navigator>)


export default class Index extends Component{
    constructor(props){
        super(props)
        this.state={
            isLogin:false
        }
       
    }
    authContext(){
        return{
            login:()=>{
                this.setState({
                    isLogin:true
                })
            }
        }
}
    render(){
        
        return(
            <AuthContext.Provider value={this.authContext()}>
                <NavigationContainer>
                    <RootScreen isLogin={this.state.isLogin}/>
                   
                </NavigationContainer>
            </AuthContext.Provider>
        )
    }
}
