import React,{Component} from "react";
import { StyleSheet, Text, View } from "react-native";

export default class AddScreen extends Component {
  render(){
      return (
    <View style={styles.container}>
      <Text>Menambahkan Halaman</Text>
    </View>
  );
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#BDB76B",
    alignItems: "center",
    justifyContent: "center"
  }
});
