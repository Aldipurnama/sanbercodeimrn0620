import React,{Component} from 'react';
import{
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    FlatList,
    Button,
    
} from 'react-native'
// import {AuthContext}
import Icon from "react-native-vector-icons/MaterialIcons";
export default class AboutScreen extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.navbar}>
                    <Icon name="arrow-back" size={25} onPress={()=>{this.props.navigation.goBack()}}></Icon>
                </View>
                <View style={styles.displayPicture}>
                    <Image source={require("./images/Profil.png")}></Image>
                    <Text style={{fontSize:17,fontFamily:'Roboto'}}>Aldi Purnama</Text>
                    <Text style={{fontSize:12,fontFamily:'Roboto',marginTop:20, fontWeight:'100'}}>Seorang Mobile Developper</Text>
                    <Text style={{fontSize:12,fontFamily:'Roboto',marginTop:20, fontWeight:'100'}}>Kadang Menjadi Perindu!</Text>
                </View>
                <View style={{...styles.displayPicture,flexDirection:'row',justifyContent:"center",marginTop:20}}>
                    <Image style={{marginHorizontal:5}} source={require('./images/Twitter.png')}></Image>
                    <Image style={{marginHorizontal:5}} source={require('./images/Facebook.png')}></Image>
                    <Image style={{marginHorizontal:5}} source={require('./images/Instagram.png')}></Image>
                </View>
            </View>
        )
    }
}

const styles=StyleSheet.create({
    container:{
        flex:1
    },
    navbar:{
        margin:25,
    },
    displayPicture:{
        alignItems:'center'
    }
})
