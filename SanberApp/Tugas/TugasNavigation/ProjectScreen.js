import React,{Component} from "react";
import { StyleSheet, Text, View } from "react-native";

export default class ProjectScreen extends Component {
  render(){
      return (
    <View style={styles.container}>
      <Text>Halaman Projek</Text>
    </View>
  );
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFE4C4",
    alignItems: "center",
    justifyContent: "center"
  }
});
