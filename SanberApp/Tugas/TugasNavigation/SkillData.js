import React from 'react'
export const AuthContext=React.createContext();
export const data={
  "items":[
    {
      "id": 1,
      "skillName": "Laravel",
      "category": "Library",
      "categoryName": "Framework / Library",
      "logoUrl": require("./images/laravel.png"),
      "iconType": "MaterialCommunityIcons",
      "iconName": "react",
      "percentageProgress": "75%"
    },
    {
      "id": 2,
      "skillName": "Express JS",
      "category": "Library",
      "categoryName": "Framework / Library",
      "logoUrl": require("./images/expressjs.png"),
      "iconType": "MaterialCommunityIcons",
      "iconName": "laravel",
      "percentageProgress": "50%"
    },
    {
      "id": 3,
      "skillName": "HTML,CSS,JavaScript",
      "category": "Language",
      "categoryName": "Bahasa Pemrograman",
      "logoUrl": require('./images/htmlcssjs.png'),
      "iconType": "MaterialCommunityIcons",
      "iconName": "language-javascript",
      "percentageProgress": "80%"
    },
    {
      "id": 4,
      "skillName": "PHP",
      "category": "Language",
      "categoryName": "Bahasa Pemrograman",
      "logoUrl": require('./images/php.png'),
      "iconType": "MaterialCommunityIcons",
      "iconName": "language-python",
      "percentageProgress": "60%"
    },
    {
      "id": 5,
      "skillName": "Figma",
      "category": "Technology",
      "categoryName": "Teknologi",
      "logoUrl": require('./images/figma.png'),
      "iconType": "MaterialCommunityIcons",
      "iconName": "git",
      "percentageProgress": "60%"
    },
    {
      "id": 6,
      "skillName": "Git Lab",
      "category": "Technology",
      "categoryName": "Teknology",
      "logoUrl": require('./images/Gitlab.png'),
      "iconType": "MaterialCommunityIcons",
      "iconName": "gitlab",
      "percentageProgress": "80%"
    }
  ]
}
