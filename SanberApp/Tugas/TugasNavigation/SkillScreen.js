import React,{Component} from 'react';
import{
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    FlatList,
    ScrollView,
    Button,
    
} from 'react-native'

import Icon from "react-native-vector-icons/MaterialIcons";
import {data} from "./SkillData";
class Items extends Component{
    constructor(props){
        super(props);
    }
    getStatus(str){
        let percen=str.split("%")[0];
        return percen>75?"Advanced":(percen>50?"Intermediate":"Basic");
    }
    render(){
        let item=this.props.item.item
        return(
            <View style={styles.cardContainer}>
                <View style={styles.card}>
                <Image style={styles.image} source={item.logoUrl}/>

                </View>    
                <View style={styles.cardDeskripsi}>
                    <Image style={{height:50,width:50}} source={require('./images/Profil.png')}></Image>
                    <View style={styles.ratingCard}>
                        <Text>{item.skillName}</Text>
                        <View style={{width:`${item.percentageProgress}`,height:20,backgroundColor:'blue',borderRadius:10,marginVertical:5}}></View>
                    </View>
                    <View style={styles.classCard}>
                        <Text >({this.getStatus(item.percentageProgress)})</Text>
                        <Text style={{marginVertical:10}}>{item.percentageProgress}</Text>
                    </View>
                    <Icon name="more-vert" size={25}></Icon>
                </View>
            </View>
            
        )
    }

    
}

const styles=StyleSheet.create({
    classCard:{
        flexDirection:'column',
        alignItems:'center'
        
    },
    ratingCard:{
       flexDirection:'column',
       width:100,
    },
    cardDeskripsi:{
        padding:15,
        justifyContent:'space-between',
        flexDirection:'row'
    },
    cardContainer:{
        flex:1,
        borderColor:'black',
        margin:15,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowColor: "#000",
        shadowOpacity: 0.8,
        shadowRadius: 4,
        elevation: 5,
    },
    image:{
        marginTop:10,
        flex: 1,
        width:'100%',
        height:50,
        resizeMode:'contain'
    },
    card:{
        paddingHorizontal:10,
        height:150,
        
    },
    container:{
        flex:1
    },
    navbar:{
        margin:25,
    },
    displayPicture:{
        flexDirection:'row-reverse',
        justifyContent:'space-between',
        margin:20
    }
})
export default class SkillScreen extends Component{
    constructor(props){
        super(props);
        this.state={
            statusShow:[false,false,false],
        }
    }
    iconPress(index){
        this.setState((state,props)=>{
            state.statusShow[index]=!state.statusShow[index];
            return {
                statusShow:state.statusShow
            }
        });
    }
    render(){
        let pemrograman=data.items.filter((val,index)=>val.category=='Language');
        let Library=data.items.filter((val,index)=>val.category=='Library');
        let tools=data.items.filter(val=>val.category=='Technology')
        return(
            <ScrollView style={styles.container}>
                <View style={styles.navbar}>
                    <Icon name="arrow-back" size={25} onPress={()=>{this.props.navigation.goBack()}}></Icon>
                </View>
                <View style={styles.displayPicture}>
                    <Image source={require("./images/Profil.png")}></Image>
                    <View style={{flexDirection:'column'}}>
                        <Text style={{fontSize:17,fontFamily:'Roboto'}}>Haii ,</Text>
                        <Text style={{fontSize:17,fontFamily:'Roboto',marginTop:20, fontWeight:'bold'}}>Aldi Purnama</Text>
                    </View>
                </View>
                <View>
                    <View style={{flexDirection:'row',justifyContent:'space-between',marginVertical:5,marginHorizontal:40}}>
                        <Text>Bahasa Pemrograman</Text>
                        <Icon name={this.state.statusShow[0]?"expand-more":"expand-less"} onPress={()=>this.iconPress(0)} size={30}></Icon>
                    </View>
                    <View style={{height:this.state.statusShow[0]?null:0}}>
                    <FlatList horizontal={true}
                    data={pemrograman}
                    renderItem={(bahasa)=><Items item={bahasa}/>}
                    keyExtractor={(bahasa)=>bahasa.id.toString()}
                    >

                    </FlatList>
                    </View>
                </View>
                <View>
                    <View style={{flexDirection:'row',justifyContent:'space-between',marginVertical:5,marginHorizontal:40}}>
                        <Text>Framework</Text>
                        <Icon name={this.state.statusShow[1]?"expand-more":"expand-less"} onPress={()=>this.iconPress(1)} size={30}></Icon>
                    
                    </View>
                    <View style={{height:this.state.statusShow[1]?null:0}}>
                    <FlatList horizontal={true}
                    data={Library}
                    renderItem={(lib)=><Items item={lib}/>}
                    keyExtractor={(lib)=>lib.id.toString()}
                    >

                    </FlatList>
                    </View>
                </View>
                <View>
                    <View style={{flexDirection:'row',justifyContent:'space-between',marginVertical:5,marginHorizontal:40}}>
                        <Text>Tools</Text>
                        <Icon name={this.state.statusShow[2]?"expand-more":"expand-less"} onPress={()=>this.iconPress(2)} size={30}></Icon>
                    
                    </View>
                    <View style={{height:this.state.statusShow[2]?null:0}}>
                    <FlatList horizontal={true}
                    data={tools}
                    renderItem={(tool)=><Items item={tool}/>}
                    keyExtractor={(tool)=>tool.id.toString()}
                    >

                    </FlatList>
                    </View>
                </View>
            </ScrollView>)
    }
}
