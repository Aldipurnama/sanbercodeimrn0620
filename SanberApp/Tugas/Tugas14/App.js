import React, { Component } from 'react';
import Main from './components/Main';
import SkillScreen from './SkillScreen';

export default class App extends Component {
  render() {
    return (
      <SkillScreen />
    );
  }
}
