import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, ScrollView, StyleSheet, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Constants from 'expo-constants';
import data from './skillData.json';

export default class SkillScreen extends Component {
  renderItem(skillData) {
    return (
      <View key={skillData.id} style={styles.listItem}>
        <Icon style={styles.listIcon} name={skillData.logoUrl} size={100} />
        <View style={styles.listDesc}>
          <Text style={styles.skillName}>{skillData.skillName}</Text>
          <Text style={styles.categoryName}>{skillData.categoryName}</Text>
          <Text style={styles.percentageProgress}>{skillData.percentageProgress}</Text>
        </View>
        <Icon style={styles.listIcon} name='chevron-right' size={100} />
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar style="auto" />
        <Image source={require('./images/stiker.png')} style={styles.logo} />
        <View style={styles.profile}>
          <Icon style={styles.loginIcon} name='account-circle' size={38} />
          <View style={styles.profileText}>
            <Text style={{color: '#000000'}}>Halo!</Text>
            <Text style={{color: '#000000', fontSize: 16}}>Aldi Purnama</Text>
          </View>
        </View>
        <View style={styles.skillHeader}>
          <Text style={{color: '#000000', fontSize: 36}}>SKILL</Text>
        </View>
        <View style={styles.category}>
            <TouchableOpacity>
                <Text style={styles.forgot}>Teknologi</Text>
            </TouchableOpacity>
        </View>
        <View style={styles.category}>
            <TouchableOpacity>
                <Text style={styles.forgot}>Bahasa Pemrograman</Text>
            </TouchableOpacity>
        </View>
        <View style={styles.category}>
            <TouchableOpacity>
                <Text style={styles.forgot}>Framework / Library</Text>
            </TouchableOpacity>
        </View>
        <FlatList
          style={{width: '100%'}}
          data={data.items}
          renderItem={(skillData) => this.renderItem(skillData.item)}
          keyExtractor={(item) => String(item.id)}
          ItemSeparatorComponent={() => {return (<View style={{height: 10}} />)}} />
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    padding: 15, paddingTop: 0,
    alignItems: 'flex-start',
    paddingTop: Constants.statusBarHeight
  },
  logo: {
    width: 40,
    height: 40,
    resizeMode: 'cover',
    alignSelf: 'flex-end'
  },
  profile: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  loginIcon: {
    color: '#FFE4C4',
    marginRight: 15
  },
  profileText: {
    color: '#003366'
  },
  skillHeader: {
    width: '100%',
    marginTop: 10,
    borderBottomWidth: 4,
    borderColor: '#3EC6FF'
  },
  category: {
    width: '50%',
    marginTop: 5, marginBottom: 5,
    backgroundColor: '#FFE4C4',
    fontWeight: 'bold',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  listItem: {
    width: '100%',
    height: 120,
    padding: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#FFE4C4',
    borderRadius: 8,
    //shadow
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 8,
    elevation: 2
  },
  listIcon: {
    color: '#003366'
  },
  listDesc: {
    width: 140,
    marginLeft: 15
  },
  skillName: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#003366'
  },
  categoryName: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#3EC6FF'
  },
  percentageProgress: {
    alignSelf: 'flex-end',
    fontSize: 40,
    fontWeight: 'bold',
    color: '#fff'
  },
});
