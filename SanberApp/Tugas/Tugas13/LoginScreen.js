import React, { Component } from 'react'
import { 
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity,
    TextInput,
} from 'react-native'

export default class Login extends Component {
    render = () => {
        return (
            <View style={styles.container}>
                <Image source={require('./images/stiker.png')} resizeMode="contain" style={styles.brandImage}/>
                <Text style={styles.logo}>LOGIN</Text>
                <View style={styles.inputView}>
                    <TextInput style={styles.inputText} placeholder="Username / Email" placeholderTextColor="#000000"/>
                </View>
                <View style={styles.inputView}>
                    <TextInput secureTextEntry style={styles.inputText} placeholder="Password" placeholderTextColor="#000000"/>
                </View>
                <TouchableOpacity>
                    <Text style={styles.forgot}>Forgot Password? Calm! :-)</Text>
                </TouchableOpacity>
                <TouchableOpacity style = {styles.loginBtn}>
                    <Text style = {styles.buttonText}> LOGIN </Text>
                </TouchableOpacity>
                    <Text style={styles.orText}>OR</Text>
                <TouchableOpacity style={styles.registerBtn}>
                    <Text style = {styles.buttonText}> REGISTER </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    brandImage: {
        marginBottom:30,
        width: 90,
        height: 90
    },
    logo:{
        fontWeight:"bold",
        fontSize:30,
        color:"#000000",
        marginBottom:20
    },
    inputView: {
        width: "80%",
        backgroundColor: "#FFE4C4",
        borderRadius: 25,
        height: 50,
        marginBottom: 20,
        justifyContent: "center",
        padding:20
    },
    inputText:{
        height: 50,
        color: "black"
    },
    forgot:{
        color:"#000000",
        fontSize: 12,
        marginBottom: 20
    },
    orText:{
        color:"#000000",
        fontSize: 15,
        marginTop: 10,
    },
    loginBtn: {
        width: "80%",
        backgroundColor: "#00FF00",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
    },
    registerBtn: {
        width: "80%",
        backgroundColor: "#00FF00",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
    },
    buttonText:{
        color:"white"
    }
})
