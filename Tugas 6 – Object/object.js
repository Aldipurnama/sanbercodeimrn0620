console.log("\n")
var hai = "Soal No. 1 (Array to Object)" 
console.log(hai)
console.log("\n")

function arrayToObject(arr) {
    if (!arr.length) {
        return;
    }

    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)
    for (var i = 0; i < arr.length; i++) {
        var person = arr[i]

        var age = person[3];
        if (age == undefined) {
            age = "Invalid Birth Year"
        } else {
            if (age > thisYear) {
                age = "Invalid Birth Year"
            } else {
                age = thisYear - age
            }
        }

        var personObj = {
            firstName: person[0],
            lastName: person[1],
            gender: person[2],
            age: age,
        }
        var no = i+1
        var fullName = `${personObj.firstName} ${personObj.lastName}`
        console.log(`${no}. ${fullName}`, personObj)
    }
}


console.log("\n")
var hai = "Driver Code" 
console.log(hai)
console.log("\n")

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
/*
    1. Bruce Banner: {
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: {
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2)
/*
    1. Tony Stark: {
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: {
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case
arrayToObject([]) // ""

console.log("\n")
var hai = "Soal No. 2 (Shopping Time)" 
console.log(hai)
console.log("\n")

function shoppingTime(memberId, money) {
    var products = [
        {name: 'Sepatu brand Stacattu', price: 1500000},
        {name: 'Baju brand Zoro', price: 500000},
        {name: 'Baju brand H&N', price: 250000},
        {name: 'Sweater brand Uniklooh', price: 175000},
        {name: 'Casing Handphone', price: 50000},
    ]

    if (memberId == undefined || memberId.trim() == '') {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    }

    var cart = []
    var currentMoney = money
    for (var i = 0; i < products.length; i++) {
        var product = products[i]
        if (currentMoney >= product.price) {
            var addToCart = true
            if (addToCart) {
                cart.push(product.name)
                currentMoney -= product.price
            }
        }
    }

    var result = {
        memberId: memberId,
        money: money,
        listPurchased: cart,
        changeMoney: currentMoney,
    }

    if (!cart.length) {
        return 'Mohon maaf, uang tidak cukup'
    }
    return result
}

console.log("\n")
var hai = "Test Cases" 
console.log(hai)
console.log("\n")

console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }

console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }

console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("\n")
var hai = "NO. 3 (Naik Angkot)" 
console.log(hai)
console.log("\n")

function naikAngkot(arrPenumpang) {
    var routes = ['A', 'B', 'C', 'D', 'E', 'F'];
    var result = []
    if (!arrPenumpang.length) {
        return result;
    }

    for (var i = 0; i < arrPenumpang.length; i++) {
        var penumpang = arrPenumpang[i]
        var dataPenumpang = {
            penumpang: penumpang[0],
            naikDari: penumpang[1],
            tujuan: penumpang[2],
            bayar: 0,
        }

        var ongkos = 0
        var naik = false
        for (var j = 0; j < routes.length; j++) {
            var route = routes[j]
            if (naik) { ongkos += 2000 }
            if (route == dataPenumpang.naikDari) { naik = true }
            if (route == dataPenumpang.tujuan) { naik = false }
        }
        dataPenumpang.bayar = ongkos

        result.push(dataPenumpang)
    }

    return result
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]