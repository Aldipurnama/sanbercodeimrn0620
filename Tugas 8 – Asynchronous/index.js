// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
//Soal No. 1 (Callback Baca Buku)
console.log(""); 
console.log("Soal No. 1 (Callback Baca Buku)");
console.log("Jawaban : ");

    readBooks(10000,books[0], function(waktu) {
        if(waktu>0) {
            readBooks(waktu,books[1], function(waktu) {
                if(waktu>0) {
                    readBooks(waktu,books[2], function(waktu) {
                        if(waktu>0) {
                          return;
                        }
                    }) 
                }
            })         
        }
    })