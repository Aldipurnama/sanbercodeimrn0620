var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

// Tulis code untuk memanggil function readBooks di sini
//Soal No. 2 (Promise Baca Buku)
console.log(""); 
console.log("Soal No. 2 (Promise Baca Buku)");
console.log("Jawaban : ");

readBooksPromise(10000,books[0])
        .then(function (waktu) {
            readBooksPromise(waktu,books[1])
                .then(function (waktu) {
                    readBooksPromise(waktu,books[2])
                        .then(function () {
                        
                        })
                        .catch(function () {
                            console.log("waktu habis");
                        });
                })
                .catch(function () {
                    console.log("waktu habis");
                });
        })
        .catch(function () {
            console.log("waktu habis");
        });
