const isExist       = (...thing) => thing.indexOf(undefined) === -1 && thing.indexOf(null) === -1;
const isIdentic     = (thing1, thing2) => JSON.stringify(thing1) === JSON.stringify(thing2);
const sortFn        = (a,b) => +a === +b ? 0 : +a > +b ? 1 : -1;
const sortDescFn    = (a,b) => +a === +b ? 0: +a < +b ? 1 : -1;
const orderFn       = (n1, n2) => [n1, n2].sort(sortFn);


console.log("\n")
var hai = "NO. 1 Range" 
console.log(hai)
console.log("\n")


function range(num1, num2) {
    if(!isExist(num1, num2)) return -1;
    const [first, last] = orderFn(num1, num2);
    return Array.from({length: (last - first) + 1}, (_, index) => index + first);
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range()) 


console.log("\n")
var hai = "NO. 2 Range with Step" 
console.log(hai)
console.log("\n")


function rangeWithStep(num1, num2, step) {
    if(!isExist(num1, num2)) return -1;
    let arr = [];
    const [first, last] = orderFn(num1, num2);
    for(let i = first; i <= last; i += step) {
        arr.push(i);
    }
    return num2 >= num1 ? arr : arr.reverse();
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 5, 4))


console.log("\n")
var hai = "NO. 3 Sum of Range" 
console.log(hai)
console.log("\n")


function sum(num1, num2, step = 1) {
    const arr = rangeWithStep(num1 || 0, num2 || 0, step);
    return arr.reduce((a,b) => a + b, 0);
}

console.log(sum(1,10))
console.log(sum(5, 50, 2))
console.log(sum(15,10))
console.log(sum(20, 10, 2))
console.log(sum(1)) 
console.log(sum()) 


console.log("\n")
var hai = "NO. 4 Array Multidimensi" 
console.log(hai)
console.log("\n")


var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(data) {
    return data.reduce((a,b) => {
        const [id, name, home, tl, hobby] = b;
        a.push({ 'Nomor ID': id, 'Nama Lengkap': name, 'TTL': home + ' ' + tl,  'Hobi':hobby });
        return a;
    },[]);
}

console.log(...dataHandling(input));


console.log("\n")
var hai = "NO. 5 Balik Kata" 
console.log(hai)
console.log("\n")


function balikKata(string) {
    let reversed = '';
    for(let i = string.length - 1; i >= 0; i--) {
        reversed = reversed + string.charAt(i);
    }
    return reversed;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

console.log("\n")
var hai = "NO. 6 Metode Array" 
console.log(hai)

function dataHandling2(data) {
    console.log(data);
    const tanggal = data[3].split("/");
    switch(parseInt(tanggal[1])) {
        case 1: 
            console.log('Januari');
            break;
        case 2: 
            console.log('Februari');
            break;
        case 3: 
            console.log('Maret');
            break;
        case 4: 
            console.log('April');
            break;
        case 5: 
            console.log('Mei');
            break;
        case 6: 
            console.log('Juni');
            break;
        case 7: 
            console.log('Juli');
            break;
        case 8: 
            console.log('Agustus');
            break;
        case 9: 
            console.log('September');
            break;
        case 10: 
            console.log('Oktober');
            break;
        case 11: 
            console.log('November');
            break;
        case 12: 
            console.log('Desember');
            break;
    }
    console.log([...tanggal].sort(sortDescFn));
    console.log(tanggal.join("-"));
    console.log(new String(data[1]).slice(0,15));
}

dataHandling2(["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"])
